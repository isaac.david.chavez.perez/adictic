
import React, { useState, useEffect } from "react";
import {
  StatusBar, View
} from 'react-native';
//import Dahsboart from './app/screens/Dahsboart';
import SplashScreen from 'react-native-splash-screen';
import AppNavigationStack from  './app/navigation/AppNavigationStack'
import Shopping from  './app/services/Shopping'
import { NavigationContainer } from "@react-navigation/native";
declare const global: { HermesInternal: null | {} };


console.disableYellowBox = true;
const App = () => {
  useEffect(() => {
    Shopping.populate();
    SplashScreen.hide();
  }, []);

  return (
    <NavigationContainer>
      <StatusBar backgroundColor={"#fffa"} barStyle="dark-content" />
      <AppNavigationStack />
    </NavigationContainer>
  );
};
export default App;
