import store from '../redux/store/index';
import { setItems, setTroley } from "../redux/actions/index.js";
const Shopping = {
    populate() {
        const items: any = [
            {
                id: "1", images: [
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem1.png" },
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem1.png" },
                ],
                rating: 4.5,
                ratingCount: 594,
                oldPrice: 250, price: 220, favorite: false, 
                description: "Tennis clásicos vans old school",
                largeDescription: "Los tennis que se tomaron los 80’s están devuelta para darle un toque vieja escuela a tu look para lograr tener el mejor estilo casual.",
                availableColours: [
                    {id: 0, color: "#9FBCFF"},
                    {id: 1, color: "#FF7062"},
                    {id: 2, color: "#FFDF9B"}
                ],
                availableSizes: [
                    {id: 0, size: "XXL"},
                    {id: 1, size: "XS"},
                    {id: 2, size: "S"},
                    {id: 3, size: "M"},
                    {id: 4, size: "L"},
                    {id: 5, size: "XL"}
                ]

            },
            {
                id: "2", images: [
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem2.png" },
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem2.png" },
                ],
                rating: 4.5,
                ratingCount: 594,
                oldPrice: 250, price: 220, favorite: false, 
                description: "Tennis negros nike",
                largeDescription: "Los tennis que se tomaron los 80’s están devuelta para darle un toque vieja escuela a tu look para lograr tener el mejor estilo casual.",
                availableColours: [
                    {id: 0, color: "#9FBCFF"},
                    {id: 1, color: "#FF7062"},
                    {id: 2, color: "#FFDF9B"}
                ],
                availableSizes: [
                    {id: 0, size: "XXL"},
                    {id: 1, size: "XS"},
                    {id: 2, size: "S"},
                    {id: 3, size: "M"},
                    {id: 4, size: "L"},
                    {id: 5, size: "XL"}
                ]

            },
            {
                id: "3", images: [
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem3.png" },
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem3.png" },
                ],
                rating: 4.5,
                ratingCount: 594,
                oldPrice: 250, price: 220, favorite: false, 
                description: "Tennis deportivos nike azul oscuro",
                largeDescription: "Los tennis que se tomaron los 80’s están devuelta para darle un toque vieja escuela a tu look para lograr tener el mejor estilo casual.",
                availableColours: [
                    {id: 0, color: "#9FBCFF"},
                    {id: 1, color: "#FF7062"},
                    {id: 2, color: "#FFDF9B"}
                ],
                availableSizes: [
                    {id: 0, size: "XXL"},
                    {id: 1, size: "XS"},
                    {id: 2, size: "S"},
                    {id: 3, size: "M"},
                    {id: 4, size: "L"},
                    {id: 5, size: "XL"}
                ]

            },
            {
                id: "4", images: [
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem4.png" },
                    { imageUrl: "http://spealsas-001-site4.ftempurl.com/images/imageItem4.png" },
                ],
                rating: 4.5,
                ratingCount: 594,
                oldPrice: 250, price: 220, favorite: false, 
                description: "Airforce",
                largeDescription: "Los tennis que se tomaron los 80’s están devuelta para darle un toque vieja escuela a tu look para lograr tener el mejor estilo casual.",
                availableColours: [
                    {id: 0, color: "#9FBCFF"},
                    {id: 1, color: "#FF7062"},
                    {id: 2, color: "#FFDF9B"}
                ],
                availableSizes: [
                    {id: 0, size: "XXL"},
                    {id: 1, size: "XS"},
                    {id: 2, size: "S"},
                    {id: 3, size: "M"},
                    {id: 4, size: "L"},
                    {id: 5, size: "XL"}
                ]

            },
          

            
        ]
        store.dispatch(setItems(items));
    },
    updateItem(item:any) {
        const items = store.getState().items;
        
        const index = items.findIndex((x: any) => x.id === item.id)
        if (index > -1) {
            items[index] = item
            let newItems : any = [];
            Object.assign(newItems, items);
            store.dispatch(setItems(newItems));
        }
    },
    addToTrolley(purchase: any){
        const _purchases = store.getState().trolley;
        _purchases.push(purchase);
        let newItems : any = [];
        Object.assign(newItems, _purchases);
        store.dispatch(setTroley(newItems));
    },
    updateTrolleyItemQuantity(id: number, quantity: number){
        const _purchases = store.getState().trolley;
        const  index = _purchases.findIndex((x:any) => x.id == id) 
        if(index > -1){
            _purchases[index].quantity = quantity
            let newItems : any = [];
            Object.assign(newItems, _purchases);
            store.dispatch(setTroley(newItems));     
        }
    },
    deleteTrolleyItem(id:number){
        const _purchases = store.getState().trolley;
        const  index = _purchases.findIndex((x:any) => x.id == id) 
        if(index > -1){
            _purchases.splice(index, 1)
            let newItems : any = [];
            Object.assign(newItems, _purchases);
            store.dispatch(setTroley(newItems));    
        }
    }
}
export default Shopping;