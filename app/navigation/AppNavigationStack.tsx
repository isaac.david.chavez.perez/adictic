import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Dahsboart from '../screens/Dahsboart'
import ItemDetail from '../screens/ItemDetail'
import Trolley from '../screens/Trolley'
const Stack = createStackNavigator();
function AppNavigationStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Dahsboart" component={Dahsboart} options={{
        headerShown: false,
      }} />
    <Stack.Screen name="ItemDetail" component={ItemDetail} options={{
      headerShown: false,
    }} />
    <Stack.Screen name="Trolley" component={Trolley} options={{
      headerShown: false,
    }} />
  </Stack.Navigator>
  );
}

export default AppNavigationStack;