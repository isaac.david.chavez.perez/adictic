
import React, { useState } from 'react';
import { View, Text,  Dimensions,  TouchableOpacity, ScrollView} from "react-native";
import HeaderApp from '../components/HeaderApp';
import CircleSelect from '../components/CircleSelect';
import SizeSelect from '../components/SizeSelect';
import NumberSpinner from '../components/NumberSpinner';
import StyleSheet from '../utils/Style';
import { useNavigation } from "@react-navigation/native";
import ItemsImagesCarousel from '../components/ItemsImagesCarousel'
import Shopping from '../services/Shopping';
import { Rating } from 'react-native-elements';
import Colors from '../utils/Colors';
import Toast from 'react-native-simple-toast';

export default function ItemDetail(props: any) {
    const navigation = useNavigation();
    const [heightScreen, setHeightScreen]: any = useState(500);
    const [widthScreen, setWidthScreen]: any = useState(0);
    const [itemPurchase, setItemPurchase]: any = useState({id:0,  colorId : 0, sizeId:0, quantity: 0, item:{}});


    const [item, setItem]: any = useState(props.route.params.item);
    const setDimensions = () => {
        Dimensions.get('window')
        setHeightScreen(Dimensions.get('window').height)
        setWidthScreen(Dimensions.get('window').width)
    }
    const setColor = (colorId: number) => {
        const _itemPurchase = itemPurchase
        _itemPurchase.colorId = colorId;
        let newObj : any = {};
        Object.assign(newObj, _itemPurchase);
        setItemPurchase(newObj);
    }
    const setQuantity = (id: number,quantity: number) => {
        const _itemPurchase = itemPurchase
        _itemPurchase.quantity = quantity;
        let newObj : any = {};
        Object.assign(newObj, _itemPurchase);
        setItemPurchase(newObj);
    }
    const setSize = (sizeId: number) => {
        const _itemPurchase = itemPurchase
        _itemPurchase.sizeId = sizeId;
        let newObj : any = {};
        Object.assign(newObj, _itemPurchase);
        setItemPurchase(newObj);
    }
    const  addtoCar = () =>{
            if (itemPurchase.quantity === 0){
                Toast.show('¡Selecciona una cantidad!', Toast.SHORT);
            }else{
                const _itemPurchase = itemPurchase
                _itemPurchase.id =  Math.trunc(Math.random() *1000);
                _itemPurchase.item = item;
                Shopping.addToTrolley(_itemPurchase);
                let newObj : any = {id:0,  colorId : 0, sizeId:0, quantity: 0, item:{}};
                setItemPurchase(newObj);
                
            }
    }

    return (
        <View onLayout={(event) => {
            setDimensions();
        }} style={[StyleSheet.container, { height: heightScreen + 32 }]} >
            <HeaderApp
                leftIcon="arrow-left"
                title="Sneakers!!!"
                subTitle="Vans"
                rightIcon1="favorite"
                rightIcon2="share"
                rightIcon1Color={item.favorite ? Colors.favoriteColor : Colors.font2}
                rightIcon2Action={() => navigation.navigate("Trolley")}
            />
            < ScrollView>
                <ItemsImagesCarousel images={item.images} width={widthScreen} />
                <View style={StyleSheet.ItemDetailContainer}>
                    <View >
                        <Text style={StyleSheet.ItemDetailDescription}> {item.description}</Text>
                    </View>
                    <View style={StyleSheet.ItemDetailRantingPriceContainer} >
                        <View style={StyleSheet.ItemDetailRantingContainer} >
                            <Rating
                                imageSize={20}
                                readonly
                                type={"custom"}
                                startingValue={item.rating}
                                ratingColor={Colors.starRateColor}
                            />
                        </View>
                        <View >
                            <Text style={StyleSheet.ItemDetailtextRatingCount}>({item.ratingCount})</Text>
                        </View>
                        <View style={StyleSheet.ItemDetailPriceContainer}>
                            {item.oldPrice !== 0 && (
                                <View>
                                    <Text style={StyleSheet.ItemDetailOldPrice}>${item.oldPrice}</Text>
                                </View>
                            )}
                            <View>
                                <Text style={StyleSheet.ItemDetailPrice}>${item.price}</Text>
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={StyleSheet.ItemDetailtextLargeDescription}>
                            {item.largeDescription}
                        </Text>
                    </View>
                    <View>
                        <Text style={[StyleSheet.ItemDetailDescription, { marginTop: 10, marginLeft: 5 }]}>
                            Elige un color
                        </Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        {item.availableColours.map((availableColour: any, key: any) => (
                            <CircleSelect  id={availableColour.id} key={availableColour.id}  color={availableColour.color} selected={itemPurchase.colorId == availableColour.id } setColor={setColor} />
                        ))}
                    </View>
                    <View>
                        <Text style={[StyleSheet.ItemDetailDescription, { marginTop: 10, marginLeft: 5 }]}>
                            Elige una talla
                        </Text>
                        <View style={{ flexDirection: "row" }}>
                        {item.availableSizes.map((availableSize: any, key: any) => (
                            <SizeSelect id={availableSize.id} key={availableSize.id} size={availableSize.size} isSelect={availableSize.id == itemPurchase.sizeId}  setSize={setSize}  />
                        ))}
                        </View>
                    </View>
                    <View style={StyleSheet.shoppingContainer}>
                        <NumberSpinner id={item.id} quantity={itemPurchase.quantity} size={1} setQuantity={setQuantity} />
                        <TouchableOpacity onPress={() => {addtoCar()}}
                        style=
                        {StyleSheet.shoppingButton} >
                            <Text style={[StyleSheet.ItemDetailDescription, { color:Colors.background }]}>
                            Agregar
                        </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}
