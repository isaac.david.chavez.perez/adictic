
import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Image, Dimensions, ScrollView } from "react-native";
import HeaderApp from '../components/HeaderApp';
import ItemCard from '../components/ItemCard';
import StyleSheet from '../utils/Style';
import store from '../redux/store/index';
import Shopping from '../services/Shopping';
export default function Dahsboart() {
    const [localItems, setLocalItems]: any = useState([]);
    const [columns, setColumns]: any = useState(2);
    const [heightScreen, setHeightScreen]: any = useState(500);
    const [expoImageVisible, setExpoImageVisible  ]: any = useState(true);

    const chageFavorite = (id:number, favorite: boolean) => {
        const item = localItems.find((x: any) => x.id === id)
        if(item){
            item.favorite = favorite;
            Shopping.updateItem(item);
        }
    }
    const calculateColumns = () => {
        Dimensions.get('window')
        const _columns = Dimensions.get('window').width / (180 )
        setColumns(Math.trunc(_columns));
        setHeightScreen(Dimensions.get('window').height)
        if(Dimensions.get('window').width > Dimensions.get('window').height){
            setExpoImageVisible(false)
        }else{
            setExpoImageVisible(true)
        }

    }
    store.subscribe(() => {
        setLocalItems(store.getState().items);
    })
    useEffect(() => {
      
    }, []);
    return (
        <View onLayout={(event) => {
            calculateColumns();
        }} style={[StyleSheet.container,{height:heightScreen +32} ]} >
            <HeaderApp
                // leftIcon="arrow-left"
                title="Sneakers!!!"
            />  
            {expoImageVisible && (
                <View style={StyleSheet.expoContainer}>
                    <View style={StyleSheet.expoImageContainer}>
                        <Image
                            style={StyleSheet.expoImage}
                            source={require('../../assets/image1.png')}
                        ></Image>
                    </View>
                    <View style={StyleSheet.expoTextContainer}>
                        <Text style={StyleSheet.expoText}>Las mejores zapatillas de Madrid</Text>
                    </View>
                </View>
                ) }
                <View style={[StyleSheet.listContainer, {height: heightScreen- ( expoImageVisible?  255: 100)}]}>
                    <FlatList
                        data={localItems}
                        numColumns={columns}
                        renderItem={({ item }) => (
                            <ItemCard
                                item={item}
                                imageUrl={item.images[0].imageUrl}
                                favorite ={item.favorite}
                                description={item.description}
                                oldPrice={item.oldPrice}
                                price={item.price}
                                chageFavorite={chageFavorite}
                            />
                        )}
                        keyExtractor={item => item.id}
                        key={columns}
                    />
                </View>
            
        </View>
    )
}