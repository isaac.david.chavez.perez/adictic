
import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, TouchableOpacity,  TouchableHighlight, Image } from "react-native";
import HeaderApp from '../components/HeaderApp';
import StyleSheet from '../utils/Style';
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import Colors from '../utils/Colors';
import store from '../redux/store/index';
import { SwipeListView } from 'react-native-swipe-list-view';
import styleSheet from '../utils/Style';  
import TrolerItem from '../components/TrolerItem';
import { Icon,   } from 'react-native-elements'
import Shopping from '../services/Shopping';


export default function Trolley() {
    const navigation = useNavigation();
    const [heightScreen, setHeightScreen]: any = useState(500);
    const [widthScreen, setWidthScreen]: any = useState(0);
    const [trolleyItems, setTrolleyItems]: any = useState([])
    const setDimensions = () => {
        Dimensions.get('window')
        setHeightScreen(Dimensions.get('window').height)
        setWidthScreen(Dimensions.get('window').width)

    }
    useFocusEffect(() => {
        setTrolleyItems(store.getState().trolley);
    });

    const deleteItem =(id:number) =>{
        Shopping.deleteTrolleyItem(id);
        setTrolleyItems(store.getState().trolley);
    }

   
    const renderItem = (data: any) => (
        
            <View >
                <TrolerItem 
                id={data.item.id} 
                imageUrl={data.item.item.images[0].imageUrl}
                description ={data.item.item.description} 
                price={data.item.item.price}
                quantity={data.item.quantity}
                ></TrolerItem>
            </View>
        
    );

    const renderHiddenItem = (data: any, rowMap: any) => (
        <View style={StyleSheet.rowBack}>
            <TouchableOpacity
                style={[StyleSheet.backRightBtn ]}
                onPress={() => deleteItem(data.item.id)}
            >
                <Icon
                    size={30}
                    name='trash-outline'
                    type='ionicon'
                    color={Colors.background}  />
                    
            </TouchableOpacity>
            
        </View>
    );

    return (
        <View onLayout={(event) => {
            setDimensions();
        }} style={[styleSheet.container, { height: heightScreen + 32 }]} >
            <HeaderApp
                leftIcon="arrow-left"
                title="Carrito"
            />
            <View style={StyleSheet.troleyContainer}>
                <SwipeListView 
                    data={trolleyItems}
                    renderItem={renderItem}
                    renderHiddenItem={renderHiddenItem}
                    rightOpenValue={-90}
                    previewRowKey={'0'}
                    previewOpenValue={-40}
                    disableRightSwipe={true}
                    previewOpenDelay={3000}
                />
            </View>
        </View>
    );
}

