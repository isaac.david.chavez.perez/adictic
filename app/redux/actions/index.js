import { SET_ITEMS, SET_TROLLEY } from "../constants/action-types";
export function setItems(payload) {
    return { type: SET_ITEMS, payload }
};
export function setTroley(payload) {
    return { type: SET_TROLLEY, payload }
};

