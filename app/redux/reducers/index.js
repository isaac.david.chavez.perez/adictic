import { SET_ITEMS } from "../constants/action-types";
import { SET_TROLLEY } from "../constants/action-types";
const initialState = {
  items: [],
  trolley: [],
  
};
function rootReducer(state = initialState, action) {
  if (action.type === SET_ITEMS) {
    return Object.assign({}, state, {
      items: state.items = action.payload
    });
  }
  if (action.type === SET_TROLLEY) {
    return Object.assign({}, state, {
      trolley : state.trolley = action.payload
    });
  }
  return state;
}
export default rootReducer;