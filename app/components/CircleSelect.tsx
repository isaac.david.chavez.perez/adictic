import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
export default function CircleSelect(props: any) {
    const { id,color, selected, setColor} = props;
    return (
        <View  style={{paddingRight: 2}}>
            <TouchableOpacity onPress={() => setColor(id) }>
            <View style={{
                width: 60,
                height: 60,
                marginTop:10,
                borderRadius: 100 / 2,
                borderColor: color,
                borderWidth: selected? 1:0,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <View
                    style={{
                        width: 50,
                        height: 50,
                        borderRadius: 100 / 2,
                        backgroundColor: color,
                    }}>

                </View>
            </View>
            </TouchableOpacity>
        </View>
    )
}

