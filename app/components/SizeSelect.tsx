import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import Colors from '../utils/Colors';
export default function SizeSelect(props: any) {
    const { id, size, isSelect,setSize } = props;
    return (
        <TouchableOpacity
        onPress={() => setSize(id) }
        >
            <View style={{ flexDirection: "row" }} >
                <View style={{
                    marginLeft: 5,
                    marginRight:5,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 1,
                    borderColor: isSelect ? Colors.favoriteColor :Colors.border1,
                    marginTop: 10,
                    width: 40,
                    height: 40,
                    borderRadius: 6,
                }}>
                    <Text style={{ fontFamily: "Muli", fontSize: 16, color: Colors.font1 }}>{size}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

