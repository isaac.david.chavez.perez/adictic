import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image } from "react-native"
import styleSheet from '../utils/Style';
import Shopping from '../services/Shopping';

import Colors from '../utils/Colors';
import NumberSpinner from '../components/NumberSpinner';
export default function TrolerItem(props: any) {
    const { id, imageUrl,description,price, quantity } = props;
    const [  localQuantity, setLocalQuantity]: any = useState(quantity)
    const setQuantity = (id:number,quantity: number) => {
        Shopping.updateTrolleyItemQuantity(id,quantity);
        setLocalQuantity(quantity)
    }
    return (
        <View  style={styles.rowFront} >
            <View style={styles.rowContainer}>
                <Image
                    style={{ width: 60, height: 60,  borderRadius: 10 }}
                    source={{ uri: imageUrl }}
                />
                <View style={styles.rowSubContainer}>
                    <Text style={[{
                        fontFamily: "Muli-Bold",
                        fontWeight: "bold",
                        fontSize: 16,
                        color: Colors.font2,
                    }]} >{description}</Text>
                    <Text style={{
                        fontFamily: "Muli",
                        fontSize: 18,
                        color: Colors.font3
                    }}>${price}</Text>
                </View>
                <NumberSpinner id={id}  quantity={localQuantity} size={2} setQuantity={setQuantity} />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: "row", paddingHorizontal:10, alignItems: "center", borderRadius:5 
    },
    rowFront: {
        backgroundColor: Colors.background,
        borderColor: Colors.border1,
        borderBottomWidth: 1,
        
        borderRadius: 5,
        justifyContent: 'center',
        height: 90,
    },
    rowSubContainer:{
        flexDirection: "column", paddingLeft: 10, alignItems: "flex-start", flex: 1 
    }
})