import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from "react-native"
import { Icon } from 'react-native-elements'
import Colors from '../utils/Colors';
export default function NumberSpinner(props: any) {
    const { id, quantity, size, setQuantity } = props;
    const [localSize, setLocalSize]: any = useState(40);
    const [fontSize, setFontSizeSize]: any = useState(25);
    

    useEffect(() => {
        if(size == 1){
            setLocalSize(40)
            setFontSizeSize(25)
        }    
        if(size == 2){
            setLocalSize(30)
            setFontSizeSize(20)
        }    
    }, []);
    const borderwith = 0.3;
    const borderRadius = 10;

    return (
        <View style={{ flexDirection: "row" }}>
            <TouchableOpacity onPress={() => {
                let _value = quantity;
                if(quantity - 1 >= 0){
                _value = quantity - 1;
                }
               
                setQuantity(id,_value);
            }} >
                <View style={{
                    width: localSize,
                    height: localSize,
                    borderBottomWidth: borderwith,
                    borderTopWidth: borderwith,
                    borderTopLeftRadius: borderRadius,
                    borderBottomLeftRadius: borderRadius,
                    borderLeftWidth: borderwith,
                    borderColor: Colors.borderColor,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>

                    <Icon name='dash' color={Colors.icons} size={20} type="octicon" />
                </View>
            </TouchableOpacity>
            <View style={{
                width: localSize,
                height: localSize,
                borderTopWidth: borderwith,
                borderBottomWidth: borderwith,
                borderColor: Colors.borderColor,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style=
                    {{
                        fontFamily: "Muli",
                        fontSize: fontSize,
                        color: Colors.font2,
                    }}
                >{quantity}</Text>
            </View>
            <TouchableOpacity onPress={() => {
                let _value = quantity;
                _value = quantity + 1;
                setQuantity(id,_value);

            }} >
                <View style={{
                    width: localSize,
                    height: localSize,
                    borderColor: Colors.borderColor,
                    justifyContent: 'center',
                    borderBottomWidth: borderwith,
                    borderTopWidth: borderwith,
                    borderTopRightRadius: borderRadius,
                    borderBottomRightRadius: borderRadius,
                    borderRightWidth: borderwith,
                    alignItems: 'center'
                }}>
                    <Icon name='plus' color={Colors.icons} size={20} type="octicon" />
                </View>
            </TouchableOpacity>
        </View>

    )
}

