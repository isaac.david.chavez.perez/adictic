import React, { useState, useEffect } from 'react';
import { View, Text,  ActivityIndicator, TouchableOpacity  } from "react-native";
import StyleSheet from '../utils/Style';
import { useNavigation } from "@react-navigation/native";
import { Image  } from 'react-native-elements';
import Colors from '../utils/Colors'
import UpdateItem from  '../services/Shopping'
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../assets/fonts/config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
export default function ItemCard(props: any) {
    const navigation = useNavigation();
    const { item,  chageFavorite} = props;
    return (
        
        <View style={StyleSheet.cardItemContainer} >
            <TouchableOpacity  onPress={() =>  navigation.navigate("ItemDetail", {item})} >
            <View>
                <Image
                    source={{ uri: item.images[0].imageUrl }}
                    style={StyleSheet.cardItemImage}
                    PlaceholderContent={<ActivityIndicator />}
                ></Image>
            </View>
            <View style={StyleSheet.cardItemDetailsContainer}>
                <View style={{ width: 135 }}>
                    <Text  style={StyleSheet.cartItemDescription}>
                        {item.description}
                    </Text>
                </View>
                    <View style={StyleSheet.cardItemFavoriteContainer}>
                        <TouchableOpacity 
                        onPress={() =>  chageFavorite(item.id, !item.favorite)}
                        >
                            <Icon name={"favorite"} size={25} color={item.favorite ? Colors.favoriteColor : Colors.icons} />
                        </TouchableOpacity>
                    </View>
            </View>
            <View style={StyleSheet.cartItemPriceContainer}>
                {item.oldPrice !== 0 && (
                <View>
                    <Text style={StyleSheet.cartItemOldPrice}>${item.oldPrice} </Text>
                </View>
                )}
                <View>
                <Text style={StyleSheet.cartItemPrice}>${item.price}</Text>
                </View>
            </View>
            </TouchableOpacity>
        </View>
        
    )

}