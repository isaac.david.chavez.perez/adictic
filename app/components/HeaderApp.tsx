import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import StyleSheet from '../utils/Style';
import { useNavigation } from "@react-navigation/native";

import { Header } from 'react-native-elements';
import Colors from '../utils/Colors'
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../assets/fonts/config.json';
const Icon = createIconSetFromFontello(fontelloConfig);
export default function HeaderApp(props: any) {
    const navigation = useNavigation();

    const { title, subTitle, leftIcon, leftIconOnpress, rightIcon1, rightIcon2, rightIcon1Color, rightIcon2Action  } = props;
    return (
        <Header
            containerStyle={{
                backgroundColor: Colors.background,
                justifyContent: 'center',
            }}
            leftComponent={
                <View style={StyleSheet.leftTitleContainer}>
                    {leftIcon && (
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                        >
                            <View>
                                <Icon name={leftIcon} size={23} style={StyleSheet.leftTitleScreen} />
                            </View>
                        </TouchableOpacity>
                    )}
                </View>
            }
            centerComponent={
                <View style={StyleSheet.centenTitleContainer}>
                    <Text style={StyleSheet.titleScreen}>
                        {title}
                    </Text>
                    <Text  style={StyleSheet.subTitleScreen}>
                        {subTitle}
                    </Text>
                </View>
            }
            rightComponent={
                <View style={StyleSheet.rightTitleContainer}>
                    {rightIcon1 && (
                        <View>
                            <Icon name={rightIcon1} size={23}  style={StyleSheet.rightTitleIcon}  color={rightIcon1Color} />
                        </View>
                    )}
                     {rightIcon2 && (
                           <TouchableOpacity
                           onPress={() => rightIcon2Action()}
                       >
                        <View>
                            <Icon name={rightIcon2} size={23}  style={StyleSheet.rightTitleIcon}/>
                        </View>
                        </TouchableOpacity>
                    )}
                </View>
            }
        />
    )
}

