import React, { useState, useEffect } from 'react';
import { View, Text, ActivityIndicator, TouchableOpacity, SafeAreaView } from "react-native";
import StyleSheet from '../utils/Style';

import { Image } from 'react-native-elements';
import Colors from '../utils/Colors'
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../assets/fonts/config.json';
import Carousel from 'react-native-snap-carousel';

const Icon = createIconSetFromFontello(fontelloConfig);
export default function ItemsImagesCarousel(props: any) {
    const { item, width } = props;
    const renderItem = (item: any, index: any) => {
        return (
            <View style={{
            }}>
                <Image
                    source={{ uri: item.item.imageUrl }}
                    style={StyleSheet.carrouseImage}
                    PlaceholderContent={<ActivityIndicator />}
                ></Image>
            </View>
        );
    }
    return (
        <View style={StyleSheet.carrouseContainer}>
            <Carousel layout={'default'}
                data={props.images}
                loop={true}
                renderItem={renderItem}
                sliderWidth={width==0? 360:width}
                itemWidth={300}
                sliderHeight={250}
            />
        </View>
    )
}
