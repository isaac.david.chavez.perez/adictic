

import { StyleSheet } from 'react-native';
import Colors from '../utils/Colors'

export default StyleSheet.create({

    container: {
        backgroundColor : Colors.background
    },
    centenTitleContainer:{
        flexDirection:"column",  
        alignItems:"center"
    },
    titleScreen:{
        fontFamily: "Muli-Bold",
        fontSize: 18,
        color: Colors.font2
    },
    subTitleScreen:{
        fontFamily: "Muli",
        marginTop:-5,
        fontSize: 14,
        color: Colors.font2
    },
    leftTitleContainer:{
        paddingLeft: 5
    },
    leftTitleScreen:{
        fontFamily: "Muli-Bold",
        color: Colors.font2
    },
    rightTitleContainer:{
        paddingRight: 5,
        flexDirection:"row"
    },
    rightTitleIcon:{
        fontFamily: "Muli-Bold",
        margin:5
    
    },
    expoContainer:{
        marginTop:5,
        paddingHorizontal: 10,
        flexDirection: "column",
        backgroundColor : Colors.background
       // justifyContent: 'center',
    },
    expoImageContainer:{
        flexDirection: "row",
        justifyContent: 'center',
    },
    
    expoImage:{
        borderRadius: 9,
        width: "100%", height: 157
    },
    expoTextContainer:{
        marginTop:10,
        flexDirection: "row",
        justifyContent: 'center',
    },
    expoText:{
        fontFamily: "Muli",
        fontSize: 16,
        color: Colors.font1
    },
    listContainer:{
        width: '100%',
        flexDirection: "column",
        backgroundColor : Colors.background
    },
    cardItemContainer:{
        flexDirection: "column",
        padding:0, 
        margin:10
    },
    cardItemImage:{
        width: 160, 
        height: 160,    
        borderRadius:10, 
        borderColor:  Colors.borderColor, 
        borderWidth: 0.1 
    },
    cardItemDetailsContainer:{
        flexDirection: "row",
        marginTop:8
    },
    cardItemFavoriteContainer:{
        width:25,
        marginTop:3,
        flexDirection: "row",
        justifyContent: 'flex-end',
    },
    cartItemDescription:{
        fontFamily: "Muli-Bold",
        fontWeight: "bold",
        fontSize: 14,
        color: Colors.font2
    },
    cartItemOldPrice:{
        textDecorationLine: 'line-through',
        fontFamily: "Muli",
        fontSize: 14,
        marginRight:5,
        color: Colors.font1
        
    },
    cartItemPrice:{
        fontFamily: "Muli",
        fontSize: 14,
        color: Colors.font1
    },
    cartItemPriceContainer:{
        flexDirection: "row",
    },
    carrouseImage:{
        borderRadius: 9, 
        height: 300, 
        width: 300
    },
    carrouseContainer: { 
        marginTop: 5, 
        flexDirection: 'column', 
        justifyContent: 'center' 
    },
    ItemDetailDescription:{
        fontFamily: "Muli-Bold",
        fontWeight: "bold",
        fontSize: 19,
        color: Colors.font2
    },
    ItemDetailContainer:{
        marginTop: 10, paddingHorizontal: 10,
        marginLeft:5 
    },
    ItemDetailRantingPriceContainer:{
        flexDirection: "row" , 
        marginTop: 5, 
        paddingHorizontal: 5,
        width:"100%",
    },
    ItemDetailtextRatingCount:{
        fontFamily: "Muli-Bold",
        fontWeight: "bold",
        fontSize: 19,
        color: Colors.icons,
        marginLeft: 10
    },
    ItemDetailRantingContainer: {
        marginTop:4
    },
    ItemDetailOldPrice:{
        textDecorationLine: 'line-through',
        fontFamily: "Muli",
        fontSize: 18,
        marginRight:15,
        color: Colors.font1
    },
    ItemDetailPrice:{
        fontFamily: "Muli",
        fontSize: 18,
        color: Colors.favoriteColor
    },
    ItemDetailPriceContainer:{
        flex : 1,
        
        justifyContent: 'flex-end',
        flexDirection: "row" , 
    },
    ItemDetailtextLargeDescription:{
        fontFamily: "Muli",
        marginLeft:5,
        fontSize: 16,
        color: Colors.icons,
        marginTop:5,
    },
    shoppingContainer: {
        marginHorizontal: 5, marginTop: 15, flexDirection: "row", flex:1,  justifyContent: 'space-between',
    },
    shoppingButton: {
        width: 180,
        backgroundColor: Colors.favoriteColor,
        height: 40,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 40
    },
    troleyContainer: {
        backgroundColor: 'white',
        flex: 1,
        paddingHorizontal: 10
    },
    rowBack: {
        flexDirection: "row", paddingLeft: 70, alignItems: "center", borderRadius:5
    },
    backRightBtn: {
        alignItems: "flex-end",
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        backgroundColor: Colors.backgroundHide,
        borderRadius:5,
        width: "100%",
        height:90,
        paddingRight:30,
        right: 0,
    },
});