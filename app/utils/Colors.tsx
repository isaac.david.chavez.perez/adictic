export default {
  icons: "#CDD4E0",
  font1: "#8A97AD",
  font2: "#000000",
  font3: "#535E71",
  background: "#fff",
  backgroundHide: "#FEA0A8",
  borderColor: "#707070",
  favoriteColor: "#FF7062",
  starRateColor: "#FFF3D8",
  border1: "#F4F7FD"


};

